//
//  Game+Levels.swift
//  Finger Spin
//
//  Created by Keith Gardner on 5/16/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//

import UIKit

extension Game {
    
    func level1(tg: Int, ttl: Int) {  // Whitespace
        var go: Bool = true
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
                
                let xDist = views[i].center.x - views[j].center.x
                let yDist = views[i].center.y - views[j].center.y
                let dist = Int (sqrt((xDist * xDist) + (yDist * yDist)))
                
                if dist < Int (tileSize) + oneWhitespaceDistance {
                    go = false
                }
            }
        }
        
        if go {
            print("Good! Progress to level 2")
            next()
        }
    }
    
    func level2(tg: Int, ttl: Int) { // Overlap
        
        var values = [Bool](count:ttl, repeatedValue: false)
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
                
                if views[i].frame.intersects(views[j].frame) {
                    values[i] = true
                    values[j] = true
                }
            }
        }
        
        if !values.contains(false) {
            print("Good! Progress to level 3")
            next()
        }
    }
    
    func level3(tg: Int, ttl: Int) { // Similar Pairs
        var values = [Bool](count:ttl, repeatedValue: false)
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
                
                if views[i].frame.intersects(views[j].frame) && views[i].colorType == views[j].colorType {
                    values[i] = true
                    values[j] = true
                }
            }
        }
        
        if !values.contains(false) {
            print("Good! Progress to level 4")
            next()
        }
    }
    
    func level4(tg: Int, ttl: Int) {     // Opposite Pairs
        var values = [Bool](count:ttl, repeatedValue: false)
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
        
                if views[i].frame.intersects(views[j].frame) && views[i].colorType != views[j].colorType {
                    values[i] = true
                    values[j] = true
                }
            }
        }
        
        if !values.contains(false) {
            print("Good! Progress to level 5")
            next()
        }
    }
    
    func level5(tg: Int, ttl: Int) {     // Whites on Right Edge
        
        var go = true
    
        for tile in views {
            if tile.colorType == .White {
                
                let tileRightEdge = tile.frame.origin.x + tile.frame.width
                
                if tileRightEdge < view.frame.width {
                    go = false
                }
            }
        }
        
        if go {
            print("Good! Progress to level 6")
            next()
        }
        
    }
    
    
    func level6(tg: Int, ttl: Int) {     // Blacks on Right Edge
        var go = true
        
        for tile in views {
            if tile.colorType == .Black {
                
                let tileRightEdge = tile.frame.origin.x + tile.frame.width
                
                if tileRightEdge < view.frame.width {
                    go = false
                }
            }
        }
        
        if go {
            print("Good! Progress to level 7")
            next()
        }
    }
    
    
    func level7(tg: Int, ttl: Int) { // Whites on Top
        var go = true
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
                if views[i].colorType == .White && views[j].colorType == .Black {
        
                    if views[i].center.y + (views[i].frame.height * 0.5) > views[j].frame.origin.y {
                        go = false
                    }
                }
            }
        }
        
        if go {
            print("Good! Progress to level 8")
            next()
        }
    }

    
    
    
    
    
    func level8(tg: Int, ttl: Int) { //Blacks on Top
        var go = true
        
        for i in 0...ttl - 1 { if i == ttl - 1 { break }
            for j in i + 1...ttl - 1 {
                if views[i].colorType == .Black && views[j].colorType == .White {
                                        
                    if views[i].center.y + views[i].frame.height * 0.5 > views[j].frame.origin.y {
                        go = false
                    }
                }
            }
        }
        
        if go {
            print("Good! Progress to level 9")
            next()
        }
    }
    
    
    
    
    
}