//
//  ColorView.swift
//  final
//
//  Created by Keith Gardner on 5/4/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//

import UIKit

enum TouchState {
    case Down, Up
}

enum ColorType {
    case Black, White
}

class ColorView: UIView {
    
    var lastPt: CGPoint = CGPoint(x: 0, y: 0)
    var colorType: ColorType = .White
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        if touches.first != nil {
            if let parentViewController = parentViewController as? Game {
                parentViewController.updateState(tag, st: .Down)
                lastPt.x = center.x
                lastPt.y = center.y
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        
        if touches.first != nil {
            
            if let parentViewController = parentViewController as? Game {
                
                let threshold: CGFloat = 3
                
                if lastPt.x - center.x > threshold || lastPt.y - center.y > threshold {
                    parentViewController.updatePositions(tag)
                    lastPt.x = center.x
                    lastPt.y = center.y
                }
            }
        }
    }

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        if touches.first != nil {
            if let parentViewController = parentViewController as? Game {
                parentViewController.updateState(tag, st: .Up)
            }
        }
    }
    
}