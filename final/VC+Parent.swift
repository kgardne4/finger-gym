//
//  VC+Parent.swift
//  Finger Spin
//
//  Created by Keith Gardner on 5/14/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.nextResponder()
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}