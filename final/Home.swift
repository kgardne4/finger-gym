//
//  Home.swift
//  final
//
//  Created by Keith Gardner on 5/5/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//

import TGPControls
import UIKit

class Home: UIViewController {
    
    @IBOutlet weak var labels: TGPCamelLabels!
    @IBOutlet weak var discreteSlider: TGPDiscreteSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        discreteSlider.ticksListener = self.labels
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if let dest = segue.destinationViewController as? Game {
            dest.howMany = Int (discreteSlider.value) + 1
        }
        
    }

    
}

