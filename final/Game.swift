//
//  ViewController.swift
//  final
//
//  Created by Keith Gardner on 5/3/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//




/*
 
 0. Place Fingers
 1. Whitespace
 2. Overlap
 3. Similar Pairs
 4. Opposite Pairs
 5. Whites on Right Edge
 6. Blacks on Right Edge
 7. Whites on Top
 8. Blacks on Top
 
 home slider - should be 2 - 11, not 1 - 10

 */


import Spring
import UIKit

class Game: UIViewController {
    
    var views: [ColorView] = []
    var states: [TouchState] = []
    var positions: [CGPoint] = []
    
    var level: Int = 0
    var started: Bool = false
    var active: Bool = true
    var gameOver: Bool = false
    
    var tileSize: CGFloat = 0
    var howMany: Int = 5
    
    var menu: UIView = UIView()
    
    var oneWhitespaceDistance: Int = 140
    
    let levels: [String] = [
        "1. Warm up... spread the tiles apart from each other", // Whitespace
        "2. Now make every tile overlap with at least one other tile", // Overlap
        "3. Pair each white with another white, and each black with another black", // Similar Pairs
        "4. Pair each white with a black", // Opposite Pairs
        "5. Make all the white tiles hang off the right edge", // Whites on Right Edge
        "6. Make all the black tiles hang off the right edge", // Blacks on Right Edge
        "7. Make sure each white tile is above every black tile", // Whites on Top
        "8. Make sure each black tile is above every white tile", // Blacks on Top
        "Impressive! Try with more tiles or grab a friend."
    ]
    
    
    let shortDescriptions: [String] = [
        "Whitespace",
        "Overlap",
        "Similar Pairs",
        "Opposite Pairs",
        "Whites on Right Edge",
        "Blacks on Right Edge",
        "Whites on Top",
        "Blacks on Top",
        ""
    ]
    
    @IBOutlet weak var theOnlyLabel: SpringLabel!

    
    
    
    //MARK: override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.tag = -1
        
        layoutTiles()
        
        for _ in 0...howMany - 1 {
            states.append(.Up)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        placeMenu()
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        super.motionEnded(motion, withEvent: event)
        
        if motion == .MotionShake {
            menu.hidden = false
        }
    }
    
    
    
    

    
        
    
    
    
    
    
    
    
    //MARK: Logic
    
    func updateState(tg: Int, st: TouchState) {
        
        states[tg] = st
        
        if allDown() {
            started = true
            next()
        }
        
        if started && !allDown() {
            gameOverF()
        }
        
    }
    

    
    func updatePositions(tg: Int) {
        if !active { return }

        
        let total = views.count

        
        switch level {
            case 1: // Whitespace
                level1(tg, ttl: total)
            
            case 2: // Overlap
                level2(tg, ttl: total)
            
            case 3: // Similar Pairs
                level3(tg, ttl: total)

            case 4: // Opposite Pairs
                level4(tg, ttl: total)
            
            case 5: // Whites on Right Edge
                level5(tg, ttl: total)
            
            case 6: // Blacks on Right Edge
                level6(tg, ttl: total)

            case 7: // Whites on Top
                level7(tg, ttl: total)
            
            case 8: // Blacks on Top
                level8(tg, ttl: total)
            
            default: 0
        }
    }
    
    
    func next() {
        
        active = false
        
        theOnlyLabel.animation = "zoomOut"
        theOnlyLabel.force = 0.8
        theOnlyLabel.duration = 0.9
        theOnlyLabel.animate()
        
        _ = NSTimer.scheduledTimerWithTimeInterval(1.2, target: self, selector: #selector(Game.showNextLabel), userInfo: nil, repeats: false)
        
        var tempViews: [ColorView] = []
        
        for each in views {
            
            let tempView = ColorView(frame: each.frame)
            tempView.backgroundColor = UIColor.grayColor()
            tempView.layer.cornerRadius = each.layer.cornerRadius
            tempView.alpha = 0.8
            
            view.addSubview(tempView)
            tempViews.append(tempView)
        }
        
        for each in tempViews {
            
            UIView.animateWithDuration(0.5, animations: {
                each.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
                each.alpha = 0
            })
        }
        
    }
    
    func showNextLabel() {
        theOnlyLabel.text = levels[level]
        
        theOnlyLabel.animation = "zoomIn"
        theOnlyLabel.force = 0.8
        theOnlyLabel.duration = 1.0
        theOnlyLabel.animate()
        level += 1
        
        _ = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(Game.activate), userInfo: nil, repeats: false)

    }
    
    func activate() {
        active = true
    }
    
    
    
    
    
    
    
    
    //MARK: IBAction
    
    func quit(sender: UIButton!) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    
    //MARK: Helpers
    
    //Called to end game
    func gameOverF() {
        gameOver = true
        
        if let btn = menu.viewWithTag(1) as? UIButton {
            btn.setTitle("Restart", forState: .Normal)
        }
        
        if let lbl = menu.viewWithTag(2) as? UILabel {
            if level - 1 >= 0{
                lbl.text = "(Level \(level - 1))\n\(shortDescriptions[level - 1])"
            }
        }
        
        
        for e in views {
            e.alpha = 0.5
            view.sendSubviewToBack(e)
        }
    
        
        menu.hidden = false
        
    }
    
    //returns whether all views have fingers on them
    func allDown() -> Bool {
        
        if states.contains(.Up) {
            return false
        }
        return true
    }
    
    //lays out and hides menu
    func placeMenu() {
        
        let w = view.frame.width / 2
        let h = view.frame.height / 2
        let x = (view.frame.width / 2) - (w / 2)
        let y = (view.frame.height / 2) - (h / 2)
        
        menu = UIView(frame: CGRectMake(x, y, w, h))
        menu.layer.cornerRadius = 25
        
        //quit button
        let quit = UIButton(frame: CGRectMake(0, 5, menu.frame.width, 55))
        quit.setTitle("Quit", forState: .Normal)
        quit.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        quit.titleLabel!.font = UIFont(name: "Avenir-Light", size: 45.0)
        quit.titleLabel?.textAlignment = .Center
        quit.tag = 1
        menu.addSubview(quit)
        quit.addTarget(self, action: #selector(Game.quit(_:)), forControlEvents: .TouchUpInside)
        
        //level description and #
        let levelDescriptor = UILabel(frame: CGRectMake(0, menu.frame.height / 2, menu.frame.width, 100))
        levelDescriptor.text = "(Level \(level))\n\(shortDescriptions[level])"
        levelDescriptor.font = UIFont(name: "Avenir-Light", size: 35.0)
        levelDescriptor.textColor = UIColor.whiteColor()
        levelDescriptor.numberOfLines = 2
        levelDescriptor.textAlignment = .Center
        levelDescriptor.tag = 2
        menu.addSubview(levelDescriptor)
        
        menu.backgroundColor = UIColor.darkGrayColor()
        
        menu.hidden = true
        self.view.addSubview(menu)
    }


}

