//
//  Game+Setup.swift
//  Finger Spin
//
//  Created by Keith Gardner on 5/16/16.
//  Copyright © 2016 Keith Gardner. All rights reserved.
//

import UIKit

extension Game {
    
    //MARK: Setup
    
    func layoutTiles() {
        
        var cornerRadius: CGFloat = 30
        
        switch (howMany) {
            case 1:
                tileSize = 275
            case 2:
                tileSize = 265
            case 3:
                tileSize = 245
            case 4:
                tileSize = 190
            case 5:
                tileSize = 170
                oneWhitespaceDistance = 80
            case 6:
                tileSize = 125
                oneWhitespaceDistance = 70
            case 7:
                tileSize = 105
                oneWhitespaceDistance = 60
            case 8:
                tileSize = 80
                cornerRadius = 20
                oneWhitespaceDistance = 50
            case 9:
                tileSize = 60
                cornerRadius = 15
                oneWhitespaceDistance = 45
            case 10:
                tileSize = 100
                cornerRadius = 15
                oneWhitespaceDistance = 35
            case 11:
                tileSize = 85
                cornerRadius = 15
                oneWhitespaceDistance = 30
            default:
                tileSize = 100
        }
        
        
        
        
        for i in 0...howMany - 1 {
            
            let columnSize = self.view.frame.size.width / CGFloat(howMany)
            let x = CGFloat(i) * columnSize + ((columnSize - tileSize) / 2)
            
            let v = ColorView(frame: CGRectMake( x, (self.view.frame.size.height / 2 - (0.5 * tileSize)), tileSize, tileSize))
            
            
            if i % 2 == 0 { v.backgroundColor = UIColor.grayColor(); v.colorType = .Black }
            else { v.backgroundColor = UIColor.whiteColor(); v.colorType = .White }
            
            v.tag = i
            
            let panRec = UIPanGestureRecognizer()
            panRec.cancelsTouchesInView = false
            panRec.addTarget(self, action: #selector(Game.draggedView(_:)))
            v.addGestureRecognizer(panRec)
            
            views.append(v)
            
            v.userInteractionEnabled = true
            
            v.layer.cornerRadius = cornerRadius
            
            view.addSubview(v)
        }
        
    }
    
    func draggedView(sender:UIPanGestureRecognizer) {
        
        if gameOver { return }
        
        let translation = sender.translationInView(self.view)
        
        let xTranslation = sender.view?.center.x
        let yTranslation = sender.view?.center.y
        
        if translation.x <= 100 && translation.y <= 50 {
            sender.view?.center = CGPointMake(xTranslation! + translation.x, yTranslation! + translation.y)
            sender.setTranslation(CGPointZero, inView: self.view)
        }
    }

    
}